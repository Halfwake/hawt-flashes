import pyglet
import config

IMAGES = {"ground" : pyglet.image.load(config.ART_FOLDER + "ground.png"),
          "tail" : pyglet.image.load(config.ART_FOLDER + "tail.png"),
          "head" : pyglet.image.load(config.ART_FOLDER + "head.png"),
          "fire" : pyglet.image.load(config.ART_FOLDER + "fire.png"),
          "player" : pyglet.image.load(config.ART_FOLDER + "player.png")}
TILES = {"fire" : pyglet.image.ImageGrid(IMAGES["fire"], 1, 4)}
FRAMES = {"fire" : (pyglet.image.AnimationFrame(TILES["fire"][0], 0.10),
                    pyglet.image.AnimationFrame(TILES["fire"][1], 0.10),
                    pyglet.image.AnimationFrame(TILES["fire"][2], 0.10),
                    pyglet.image.AnimationFrame(TILES["fire"][3], 0.10))}
ANIMATIONS = {"fire" : pyglet.image.Animation(FRAMES["fire"])}

class Ground(pyglet.sprite.Sprite):
    def __init__(self, map_x, map_y):
        self.map_x = map_x
        self.map_y = map_y
        super(Ground, self).__init__(IMAGES["ground"], 0, 0)

class Tail(pyglet.sprite.Sprite):
    def __init__(self, map_x, map_y):
        self.map_x = map_x
        self.map_y = map_y
        super(Tail, self).__init__(IMAGES["tail"], 0, 0)

class Head(pyglet.sprite.Sprite):
    def __init__(self, map_x, map_y):
        self.map_x = map_x
        self.map_y = map_y
        super(Head, self).__init__(IMAGES["head"], 0, 0)

class Fire(pyglet.sprite.Sprite):
    def __init__(self, map_x, map_y):
        self.map_x = map_x
        self.map_y = map_y
        super(Fire, self).__init__(ANIMATIONS["fire"], 0, 0)

class Player(pyglet.sprite.Sprite):
    def __init__(self, map_x, map_y):
        self.map_x = map_x
        self.map_y = map_y
        super(Player, self).__init__(IMAGES["player"], 0, 0)
