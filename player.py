import pyglet
import tile
import config

class Player(tile.Player):
    def __init__(self, map_x, map_y):
        super(Player, self).__init__(map_x, map_y)
        self.stuck = False
        self.speed = config.PLAYER_START_SPEED
    def draw(self):
        self.x = (self.map_x * config.TILE_WIDTH) + config.X_MARGIN
        self.y = (self.map_y * config.TILE_HEIGHT) + config.Y_MARGIN
        super(Player, self).draw()
    def clear_inter(self):
        "Clears the tween sprite so this doesn't look weird."
        self.inter_pos = (None, None)
    def move(self, keys):
        k = pyglet.window.key
        if not self.stuck:
            self.old_pos = (self.map_x, self.map_y)
            if self.map_y < (config.GAME_HEIGHT - 1):
                if config.UP in keys:
                        self.map_y += 1
            if self.map_y  > 0:
                if config.DOWN in keys:
                    self.map_y -= 1
            if self.map_x > 0:
                if config.LEFT in keys:
                    self.map_x -= 1
            if self.map_x < (config.GAME_WIDTH - 1):
                if config.RIGHT in keys:
                    self.map_x += 1
