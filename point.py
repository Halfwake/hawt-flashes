import pyglet
import config
import image

class Point(pyglet.sprite.Sprite):
    def __init__(self, map_x, map_y):
        super(Point, self).__init__(image.ANIMATIONS["point1"], 0, 0)
        self.map_x = map_x
        self.map_y = map_y
    def draw(self):
        self.set_position(self.map_x * config.TILE_WIDTH + config.X_MARGIN,
                          self.map_y * config.TILE_HEIGHT + config.Y_MARGIN)
        super(Point, self).draw()
    def touching(self, other):
        if self.map_x == other.map_x and self.map_y == other.map_y:
            return True
        else:
            return False
