import config

class Messenger(object):
    "Used to allow objects to communicate with each other."
    mainGame = None
    gameScreen = None
    menuScreen = None
    creditScreen = None
    score = 0
    speed = config.MONSTER_START_SPEED
    monsters = config.START_MONSTERS
    @staticmethod
    def change_mode(new_mode):
        "Changes game mode, makes me wish we had symbols in Python."
        Messenger.mainGame.mode = Messenger.mainGame.modes[new_mode]
    @staticmethod
    def quit():
        Messenger.game.on_close()
