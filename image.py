import pyglet
import config

IMAGES = {"game_bg" : pyglet.image.load(config.ART_FOLDER + "game_overlay.png"),
          "menu_bg" : pyglet.image.load(config.ART_FOLDER + "menu_overlay.png"),
          "point1" : pyglet.image.load(config.ART_FOLDER + "point1.png"),
          "start_button" : pyglet.image.load(config.ART_FOLDER + "start_button.png"),
          "quit_button" : pyglet.image.load(config.ART_FOLDER + "quit_button.png")}

TILES = {"point1" : pyglet.image.ImageGrid(IMAGES["point1"], 1, 4)}

FRAMES = {"point1" : (pyglet.image.AnimationFrame(TILES["point1"][0], 0.10),
                      pyglet.image.AnimationFrame(TILES["point1"][1], 0.10),
                      pyglet.image.AnimationFrame(TILES["point1"][2], 0.10),
                      pyglet.image.AnimationFrame(TILES["point1"][3], 0.10))}

ANIMATIONS = {"point1" : pyglet.image.Animation(FRAMES["point1"])}
