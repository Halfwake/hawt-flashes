import tile
import config

class Fire(tile.Fire):
    def __init__(self, map_x, map_y):
        super(Fire, self).__init__(map_x, map_y)
    def draw(self):
        self.x = self.map_x * config.TILE_WIDTH + config.X_MARGIN
        self.y = self.map_y * config.TILE_HEIGHT + config.Y_MARGIN
        super(Fire, self).draw()
