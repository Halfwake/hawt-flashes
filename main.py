import pyglet
import messenger
import config
import menu
import game
import sound
from pyglet.gl import *

class MainGame(pyglet.window.Window):
    def __init__(self, width, height):
        messenger.Messenger.mainGame = self
        super(MainGame, self).__init__(width, height, vsync = True)
        pyglet.clock.set_fps_limit(config.FPS)
        self.music_player = None
        self.create_music_player()
        self.fps_display = pyglet.clock.ClockDisplay()
        self.modes = {"MenuScreen" : menu.MenuScreen(),
                      "GameScreen" : game.GameScreen(),}
        #self.mode = self.modes["GameScreen"] #testing purposes, should default to menu
        self.mode = self.modes["MenuScreen"]
    def create_music_player(self):
        self.music_player = pyglet.media.Player()
        self.music_player.queue(sound.SOUNDS["africa1"])
        self.music_player.eos_action = "loop"
        self.music_player.play()
    def on_close(self):
        self.music_player.pause()
        super(MainGame, self).on_close()
    def on_draw(self):
        self.clear()
        self.mode.on_draw()
        self.fps_display.draw()
    def on_key_press(self, symbol, modifiers):
        self.mode.on_key_press(symbol, modifiers)
    def on_key_release(self, symbol, modifiers):
        self.mode.on_key_release(symbol, modifiers)
    def on_mouse_motion(self, x, y, dx, dy):
        self.mode.on_mouse_motion(x, y, dx, dy)
    def on_mouse_press(self, x, y, button, modifiers):
        self.mode.on_mouse_press(x, y, button, modifiers)
    def on_mouse_release(self, x, y, button, modifiers):
        self.mode.on_mouse_release(x, y, button, modifiers)
    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        self.mode.on_mouse_drag(x, y, dx, dy, buttons, modifiers)

if __name__ == "__main__":
    root = MainGame(config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
    root.set_caption("Hawt Flashes")
    glEnable(GL_BLEND)
    pyglet.app.run()
    
