import pyglet
import config

SOUNDS = {"pop" : pyglet.media.load(config.MUSIC_FOLDER + "matches-1.mp3",
                                    streaming = False),
          "ring" : pyglet.media.load(config.MUSIC_FOLDER + "bell-ring-01.mp3",
                                     streaming = False),
          "africa1" : pyglet.media.load(config.MUSIC_FOLDER + "Dubakupado.mp3",
                                        streaming = True),
          "coin" : pyglet.media.load(config.MUSIC_FOLDER + "button-4.mp3",
                                        streaming = False)}
