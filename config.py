"""A collection of constants that creates an
easy way to configure in game values. Prevents
magic numbers.""" 
import os
from pyglet.window.key import *

FONT_NAME = "arial"

SCREEN_WIDTH = 768
SCREEN_HEIGHT = 544

GAME_WIDTH = 20
GAME_HEIGHT = 13

TILE_WIDTH = 32
TILE_HEIGHT = 32

X_MARGIN = 64
Y_MARGIN = 64

ART_FOLDER = "resources" + os.sep + "art" + os.sep
MUSIC_FOLDER = "resources" + os.sep + "music" + os.sep

FPS = 40

UP = W
DOWN = S
LEFT = A
RIGHT = D

MONSTER_START_SPEED = 3
POP_PER_SECOND = 3
START_MONSTERS = 3

PLAYER_START_SPEED = 10
