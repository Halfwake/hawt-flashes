import pyglet
import random
import config
import screen
import messenger
import tile
import monster
import player
import fire
import point
import sound
import image

class GameScreen(screen.AbstractScreen):
    def __init__(self):
        messenger.Messenger.gameScreen = self
        self.score = messenger.Messenger.score
        self.background = image.IMAGES["game_bg"]
        self.keys = set()
        self.tile_batch = None
        self.tile_map = None
        self.new_map()
        self.monsters = []
        self.monster_count = 0
        self.player = None
        self.fire = None
        self.score_label = None
        self.point = None
        self.spawn_monsters()
        self.place_player()
        self.make_hud()
        self.new_fire()
        self.new_point()
        self.updaters = [(lambda dt : self.monster_search(), 1.0 / messenger.Messenger.speed),
                         (lambda dt : self.player.move(self.keys), 1.0 / self.player.speed),
                         (lambda dt : self.check_for_point(), 0.01),
                         (lambda dt : self.update_hud(), 0.01)]
        self.new_updaters()
    def make_hud(self):
        self.score_label = pyglet.text.Label(text = "",
                                             font_name = config.FONT_NAME,
                                             font_size = 32,
                                             x = config.SCREEN_WIDTH / 8 * 2,
                                             y = config.SCREEN_HEIGHT / 30 * 28)
    def update_hud(self):
        self.score_label.text = "Score: %d" % self.score
    def place_player(self):
        taken_pos = self.taken_positions()
    
        while True:
            x = random.choice(range(config.GAME_WIDTH))
            y = random.choice(range(config.GAME_HEIGHT))
            if (x, y) not in taken_pos:
                self.player = player.Player(x, y)
                break
    def taken_positions(self):
        taken_pos = set()
        for monster in self.monsters:
            for item in monster.tail_links:
                taken_pos.add((item.map_x, item.map_y))
            head = monster.head
            taken_pos.add((head.map_x, head.map_y))
        if self.player: taken_pos.add((self.player.map_x, self.player.map_y))
        if self.point: taken_pos.add((self.point.map_x, self.point.map_y))
        if self.fire: taken_pos.add((self.fire.map_x, self.fire.map_y))
        return list(taken_pos)
    def collect_point(self):
        self.add_score(250)
        self.new_point()
    def check_for_point(self):
        if self.point:
            if self.point.touching(self.player):
                self.collect_point()
    def add_score(self, amount):
        self.score += amount
        pyglet.clock.schedule_once(lambda dt : sound.SOUNDS["ring"].play(), 0.01)
    def spawn_monsters(self):
        "Spans the appropriate amount of monsters for the level."
        for i in range(messenger.Messenger.monsters):
            self.add_monster()
    def add_monster(self):
        "Adds one monster to the game."
        self.monster_count += 1
        new_monster = monster.Monster(messenger.Messenger.speed)
        self.monsters.append(new_monster)
    def monster_search(self):
        "Tells the monster instance to search for the fire."
        for monster in self.monsters:
            monster.search(self.fire)
            if monster.touching_flame(self.fire):
                self.new_fire()
                monster.add_tail()
    def new_updaters(self):
        "Schedules all functions in self.updaters."
        for lambda_pair in self.updaters:
            pyglet.clock.schedule_interval(lambda_pair[0],
                                           lambda_pair[1])
    def del_updaters(self):
        "Unschedules all functions in self.updaters."
        for lambda_pair in self.updaters:
            pyglet.clock.unschedule(lambda_pair[0])
    def __del__(self):
        self.del_updaters()
    def new_point(self):
        taken_pos = self.taken_positions()
        
        while True:
            x = random.choice(range(config.GAME_WIDTH))
            y = random.choice(range(config.GAME_HEIGHT))
            if (x, y) not in taken_pos:
                self.point = point.Point(x, y)
                break
    def new_fire(self):
        taken_pos = self.taken_positions()
        
        while True:
            x = random.choice(range(config.GAME_WIDTH))
            y = random.choice(range(config.GAME_HEIGHT))
            if (x, y) not in taken_pos:
                self.fire = fire.Fire(x, y)
                break
    def new_map(self):
        "Resets this objects game map."
        H = config.GAME_HEIGHT
        W = config.GAME_WIDTH
        self.tile_batch = pyglet.graphics.Batch()
        self.tile_map = [[tile.Ground(x, y) for y in range(H)]for x in range(W)]
        for column in self.tile_map:
            for item in column:
                item.batch = self.tile_batch
    def map_draw(self):
        for x, column in enumerate(self.tile_map):
            for y, item in enumerate(column):
                item.set_position(x * config.TILE_WIDTH + config.X_MARGIN,
                                  y * config.TILE_HEIGHT + config.Y_MARGIN)
        self.tile_batch.draw()
    def on_draw(self):
        self.map_draw()
        self.background.blit(0, 0)
        self.score_label.draw()
        self.point.draw()
        self.player.draw()
        self.fire.draw()
        for monster in self.monsters:
            monster.draw()
    def on_key_press(self, symbol, modifiers):
        self.keys.add(symbol)
    def on_key_release(self, symbol, modifiers):
        self.keys.remove(symbol)
