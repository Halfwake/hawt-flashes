import pyglet
import random
import tile
import config
import messenger
import sound

class Monster(object):
    def __init__(self, speed):
        self.head = tile.Head(random.choice(range(config.GAME_WIDTH)),
                              random.choice(range(config.GAME_HEIGHT)))
        self.tail_batch = pyglet.graphics.Batch()
        self.tail_links = []
        self.new_link_pos = (None, None)
        self.speed = speed
        self.killed = False
        self.pop_per_second = config.POP_PER_SECOND
    def kill(self):
        messenger.Messenger.gameScreen.monster_count -= 1
        self.killed = True
        length = len(self.tail_links)
        for i in range(length):
            pyglet.clock.schedule_once(lambda dt : self.del_tail(), i * (1.0 / self.pop_per_second))
            pyglet.clock.schedule_once(lambda dt : sound.SOUNDS["pop"].play(), i  * (1.0 / self.pop_per_second))
            pyglet.clock.schedule_once(lambda dt : messenger.Messenger.gameScreen.add_score(100), i  * (1.0 / self.pop_per_second))
            pyglet.clock.schedule_once(lambda dt : messenger.Messenger.mainGame.music_player.play(), (length + 1) * (1.0 / self.pop_per_second))
        if messenger.Messenger.gameScreen.monster_count == 0:
            messenger.Messenger.mainGame.music_player.pause()
            messenger.Messenger.gameScreen.player.stuck = True
            pyglet.clock.schedule_once(lambda dt : messenger.Messenger.change_mode("MenuScreen"), (length + 1) * (1.0 / self.pop_per_second))
        pyglet.clock.schedule_once(lambda dt : messenger.Messenger.gameScreen.monsters.pop(messenger.Messenger.gameScreen.monsters.index(self)), (length + 1) * (1.0 / self.pop_per_second))
    def draw(self):
        self.draw_head()
        self.draw_tail()
    def draw_head(self):
        self.head.set_position(self.head.map_x * config.TILE_WIDTH + config.X_MARGIN,
                               self.head.map_y * config.TILE_HEIGHT + config.Y_MARGIN)
        self.head.draw()
    def draw_tail(self):
        for link in self.tail_links:
            link.set_position(link.map_x * config.TILE_WIDTH + config.X_MARGIN,
                              link.map_y * config.TILE_HEIGHT + config.Y_MARGIN)
        self.tail_batch.draw()
    def del_tail(self):
        if self.tail_links:
            self.tail_links.pop()
    def add_tail(self):
        pos = self.new_link_pos
        assert pos != (None, None), "Creature has not had a chance to move."
        new_tail = tile.Tail(pos[0], pos[1])
        new_tail.batch = self.tail_batch
        self.tail_links.append(new_tail)
        pyglet.clock.schedule_once(lambda dt : sound.SOUNDS["pop"].play(), 0.01)
    def search(self, flame):
        "A search for a fire."
        if self.touching_flame(flame):
            self.add_tail()
            
        x, y = 0, 0
        #Handles movement along the axises.
        if self.head.map_x < flame.map_x:
            x = 1
        elif self.head.map_x > flame.map_x:
            x = -1
        if self.head.map_y < flame.map_y:
            y = 1
        elif self.head.map_y > flame.map_y:
            y = -1

        #prevent diagnol movement
        if x != 0 and y != 0:
            if random.randrange(2):
                x = 0
            else:
                y = 0
        if (x, y) != (0, 0) and (x, y) in self.get_adjacent_set():
            self.move(x, y)
        else:
            self.desperate_search()
    def desperate_search(self):
        adjacent = self.get_adjacent_set()
        if len(adjacent) < 1:
            if not self.killed:
                self.kill()
        else:
            x, y = random.choice(list(adjacent))
            self.move(x, y)
        
    def get_adjacent_set(self):
        "A search for any clear adjacent tile."
        adjacent = set()
        for i in range(-1,2):
            if i == 0:
                pass
            else:
                adjacent.add(((self.head.map_x + i, self.head.map_y),
                              (i, 0)))
                adjacent.add(((self.head.map_x, self.head.map_y + i),
                              (0, i)))
        to_remove = []
        for element in adjacent:
            if element[0][0] < 0 or element[0][0] > config.GAME_WIDTH - 1:
                to_remove.append(element)
            elif element[0][1] < 0 or element[0][1] > config.GAME_HEIGHT - 1:
                to_remove.append(element)
        for monster in messenger.Messenger.gameScreen.monsters:
            head_pos = (monster.head.map_x, monster.head.map_y)
            for element in adjacent:
                if head_pos == element[0]:
                    to_remove.append(element)
            for element in adjacent:
                for link in monster.tail_links:
                    if (link.map_x, link.map_y) == element[0]:
                        to_remove.append(element)
        for element in to_remove:
            adjacent.remove(element)
        relative_adj = [elem[1] for elem in adjacent]
        return relative_adj
    def move(self, x, y):
        assert -1 <= x <= 1, "Invalid 'x' value"
        assert -1 <= y <= 1, "Invalid 'y' value"

        if self.killed == True:
            return

        if self.tail_links:
            end_tail = self.tail_links.pop()
            self.new_link_pos = (end_tail.map_x, end_tail.map_y)
            front_tail = tile.Tail(self.head.map_x, self.head.map_y)
            front_tail.batch = self.tail_batch
            self.tail_links.insert(0, front_tail)
        else:
            self.new_link_pos = (self.head.map_x, self.head.map_y)

        

        self.head.map_x += x
        self.head.map_y += y
    def touching_flame(self, flame):
        if self.head.map_x == flame.map_x and self.head.map_y == flame.map_y:
            return True
        else:
            return False
    def touching_player(self, player):
        if self.head.map_x == player.map_x and self.head.map_y == player.map_y:
            return True
        for link in self.tail_links:
            if link.map_x == player.map_x and link.map_y == player.map_y:
                return True
        return False
