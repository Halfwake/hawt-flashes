import pyglet

import config
import screen
import messenger
import image
import button

class MenuScreen(screen.AbstractScreen):
    "A subclass for the main menu."
    def __init__(self):
        messenger.Messenger.menuScreen = self
        self.background = image.IMAGES["menu_bg"]
        self.make_buttons()
    def make_buttons(self):
        self.buttons = []
        
        start_button = button.Button(image.IMAGES["start_button"], config.SCREEN_WIDTH / 4, config.SCREEN_HEIGHT / 4 * 2)
        start_button.command = lambda : messenger.Messenger.change_mode("GameScreen")
        self.buttons.append(start_button)
        
        quit_button = button.Button(image.IMAGES["quit_button"], config.SCREEN_WIDTH / 4 , config.SCREEN_HEIGHT / 4 * 1)
        quit_button.command = lambda : quit()
        self.buttons.append(quit_button)
    def on_draw(self):
        self.background.blit(0, 0)
        for button in self.buttons: button.draw()
    def on_mouse_press(self, x, y, button, modifiers):
        if button == pyglet.window.mouse.LEFT:
            for button in self.buttons: button.click(x, y)
